/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.1.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QLabel *label;
    QPushButton *SLOC;
    QPlainTextEdit *plainTextEdit;
    QPushButton *EmptyLines;
    QLabel *label_2;
    QLabel *label_3;
    QPushButton *Comments;
    QLabel *label_5;
    QPushButton *OnlyCode;
    QLabel *label_4;
    QPushButton *VocOperandn2;
    QLabel *label_6;
    QPushButton *VocOperatn1;
    QLabel *label_7;
    QPushButton *AllOperandN2;
    QLabel *label_8;
    QPushButton *ALLOperatN1;
    QLabel *label_9;
    QPushButton *Cyclomatic;
    QLabel *label_10;
    QPushButton *N;
    QLabel *label_11;
    QPushButton *V;
    QLabel *label_12;
    QPushButton *Voc;
    QLabel *label_13;
    QPushButton *pushButton;
    QLabel *label_14;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(879, 453);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(120, 195, 46, 16));
        SLOC = new QPushButton(centralWidget);
        SLOC->setObjectName(QStringLiteral("SLOC"));
        SLOC->setGeometry(QRect(30, 190, 75, 23));
        plainTextEdit = new QPlainTextEdit(centralWidget);
        plainTextEdit->setObjectName(QStringLiteral("plainTextEdit"));
        plainTextEdit->setGeometry(QRect(30, 10, 551, 171));
        EmptyLines = new QPushButton(centralWidget);
        EmptyLines->setObjectName(QStringLiteral("EmptyLines"));
        EmptyLines->setGeometry(QRect(200, 190, 75, 23));
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(290, 190, 46, 16));
        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(290, 220, 46, 16));
        Comments = new QPushButton(centralWidget);
        Comments->setObjectName(QStringLiteral("Comments"));
        Comments->setGeometry(QRect(200, 220, 75, 23));
        label_5 = new QLabel(centralWidget);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(340, 220, 51, 16));
        OnlyCode = new QPushButton(centralWidget);
        OnlyCode->setObjectName(QStringLiteral("OnlyCode"));
        OnlyCode->setGeometry(QRect(200, 250, 75, 23));
        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(290, 250, 46, 20));
        VocOperandn2 = new QPushButton(centralWidget);
        VocOperandn2->setObjectName(QStringLiteral("VocOperandn2"));
        VocOperandn2->setGeometry(QRect(30, 290, 81, 23));
        label_6 = new QLabel(centralWidget);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(120, 290, 46, 16));
        VocOperatn1 = new QPushButton(centralWidget);
        VocOperatn1->setObjectName(QStringLiteral("VocOperatn1"));
        VocOperatn1->setGeometry(QRect(200, 290, 75, 23));
        label_7 = new QLabel(centralWidget);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(290, 290, 46, 16));
        AllOperandN2 = new QPushButton(centralWidget);
        AllOperandN2->setObjectName(QStringLiteral("AllOperandN2"));
        AllOperandN2->setGeometry(QRect(30, 320, 75, 23));
        label_8 = new QLabel(centralWidget);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(290, 320, 46, 16));
        ALLOperatN1 = new QPushButton(centralWidget);
        ALLOperatN1->setObjectName(QStringLiteral("ALLOperatN1"));
        ALLOperatN1->setGeometry(QRect(200, 320, 75, 23));
        label_9 = new QLabel(centralWidget);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setGeometry(QRect(120, 320, 46, 16));
        Cyclomatic = new QPushButton(centralWidget);
        Cyclomatic->setObjectName(QStringLiteral("Cyclomatic"));
        Cyclomatic->setGeometry(QRect(410, 190, 75, 23));
        label_10 = new QLabel(centralWidget);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setGeometry(QRect(500, 190, 46, 16));
        N = new QPushButton(centralWidget);
        N->setObjectName(QStringLiteral("N"));
        N->setGeometry(QRect(430, 250, 31, 23));
        label_11 = new QLabel(centralWidget);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setGeometry(QRect(500, 220, 46, 16));
        V = new QPushButton(centralWidget);
        V->setObjectName(QStringLiteral("V"));
        V->setGeometry(QRect(430, 280, 31, 23));
        label_12 = new QLabel(centralWidget);
        label_12->setObjectName(QStringLiteral("label_12"));
        label_12->setGeometry(QRect(500, 250, 46, 16));
        Voc = new QPushButton(centralWidget);
        Voc->setObjectName(QStringLiteral("Voc"));
        Voc->setGeometry(QRect(430, 220, 31, 23));
        label_13 = new QLabel(centralWidget);
        label_13->setObjectName(QStringLiteral("label_13"));
        label_13->setGeometry(QRect(500, 280, 46, 16));
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(430, 310, 31, 23));
        label_14 = new QLabel(centralWidget);
        label_14->setObjectName(QStringLiteral("label_14"));
        label_14->setGeometry(QRect(500, 310, 46, 16));
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 879, 21));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        label->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        SLOC->setText(QApplication::translate("MainWindow", "SLOC", 0));
        EmptyLines->setText(QApplication::translate("MainWindow", "EmptyLines", 0));
        label_2->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        label_3->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        Comments->setText(QApplication::translate("MainWindow", "Comments", 0));
        label_5->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        OnlyCode->setText(QApplication::translate("MainWindow", "OnlyCode", 0));
        label_4->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        VocOperandn2->setText(QApplication::translate("MainWindow", "VocOperandn2", 0));
        label_6->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        VocOperatn1->setText(QApplication::translate("MainWindow", "VocOperatn1", 0));
        label_7->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        AllOperandN2->setText(QApplication::translate("MainWindow", "AllOperandN2", 0));
        label_8->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        ALLOperatN1->setText(QApplication::translate("MainWindow", "ALLOperatN1", 0));
        label_9->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        Cyclomatic->setText(QApplication::translate("MainWindow", "Cyclomatic", 0));
        label_10->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        N->setText(QApplication::translate("MainWindow", "N", 0));
        label_11->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        V->setText(QApplication::translate("MainWindow", "V", 0));
        label_12->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        Voc->setText(QApplication::translate("MainWindow", "Voc", 0));
        label_13->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        pushButton->setText(QApplication::translate("MainWindow", "CI", 0));
        label_14->setText(QApplication::translate("MainWindow", "TextLabel", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
